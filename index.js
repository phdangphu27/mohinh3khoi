// bai1

// input: days

// step 1: tạo biến days cho số ngày đi làm và biến salary để lưu kết quả lương
// step 2: cho giá trị biến days
// step 3: salary = days * 100000
// step 4: in kết quả ra console

// output: salary

var days = 10;

var salary = days * 100000;

console.log("salary:", salary);

///////////////////////////////////////////////////////////////////////////////////////

// bai2

// input: firstNum ,secondNum , thirdNum , fourthNum , fifthNum

// step 1: tạo 5 biến cho 5 số khác nhau
// step 2: tạo biến calcAvg để lưu kết quả trung bình
// step 3: cho 5 giá trị
// step 4: calcAvg = (firstNum + secondNum +  thirdNum +  fourthNum +  fifthNum)/5
// step 5: in kết quả ra console

// output: calcAvg

var firstNum = 1,
  secondNum = 2,
  thirdNum = 3,
  fourthNum = 4,
  fifthNum = 5;

var calcAvg = (firstNum + secondNum + thirdNum + fourthNum + fifthNum) / 5;

console.log("calcAvg:", calcAvg);

// bai 3

// input: usd

// step 1: tạo biến usd
// step 2: tạo biến vnd
// step 3: cho giá trị biến usd
// step 4: vnd = usd * 23500
// step 5: in kết quả ra console

// output: vnd

var usd = 10;

var vnd = usd * 23500;

console.log("vnd:", vnd);

///////////////////////////////////////////////////////////////////////////////////////

// bai 4

// input : firstEdge , secondEdge

// step 1: tạo biến firstEdge , secondEdge cho 2 cạnh của hình chữ nhật
// step 2: tạo biến perimiter , area
// step 3: cho giá trị biến firstEdge , secondEdge
// step 4: perimiter = (firstEdge + secondEdge) * 2
// step 5: area = firstEdge * secondEdge
// step 6: in kết quả ra console

// output : perimeter

var firstEdge = 1,
  secondEdge = 2;

var perimiter = (firstEdge + secondEdge) * 2;

var area = firstEdge * secondEdge;

console.log("perimiter:", perimiter);
console.log("area:", area);

// bai 5

// input: number

// step 1: tạo biến number và firstDigit , secondDigit ,total
// step 2: cho giá trị biến number
// step 3: firstDigit = Math.floor(number / 10)
// step 4: secondDigit = number % 10
// step 5: in kết quả ra console

// output:

var number = 12,
  firstDigit,
  secondDigit,
  total;

firstDigit = Math.floor(number / 10);

secondDigit = number % 10;

total = firstDigit + secondDigit;

console.log("total:", total);
